# Newman Test
This repository is a demo on how to use Postman/Newman and run it on the CI/CD pipeline.

### Installation
`npm run install` - To install all npm packages

### Node Modules
- [Newman](https://github.com/postmanlabs/newman)
- [newman-reporter-htmlextra](https://www.npmjs.com/package/newman-reporter-htmlextra)

### NPM commands
`npm run test` - To run newman and the postman collection <br>
`npm run test:report` - To run newman and generate html and json report